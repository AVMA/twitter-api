#  Frontend Code Challenge

## Setup

Install the project dependencies:

`npm install`

## Running

Update config.json with your Twitter API credentials.
Start the static and proxy servers:

`npm start`

This server will help you to get Twitter's API to send you tweets without you needing to do any authentication.  You don't need to modify server.js at all.

V1 of project was sent to TURING. Get project from: https://drive.google.com/open?id=1PeLky1ZuyyDmBz-7IcBvL6haWbPxD56V