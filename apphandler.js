//Keys
const fs = require('fs');
var securityKeysJSON = fs.readFileSync('config.json');
var securityKeys    = JSON.parse(securityKeysJSON);

//Server for Twitter API
var twitterProxyServer = require('twitter-proxy');
twitterProxyServer({
  consumerKey: securityKeys.consumerKey,
  consumerSecret: securityKeys.consumerSecret,
  port: securityKeys.port
});

//UI Web Application
var http = require('http');
var visits = 0;
var _environmentVars = "var _port="+securityKeys.port+";";
http.createServer(function (req, res) {    
    //Serve HTML base-page
    fs.readFile("./views/main.html", function(err,data){
        if(err){
            res.writeHead(404,{'Content-Type':'text/plain'})
            res.end('404 file not found');
        }
        else{
            res.writeHead(200,{'Content-Type':'text/html'})
            var rawFile = data;
            rawFile = rawFile.toString().replace("//@variablesSpace",_environmentVars);
            res.write(rawFile);
            res.end();
        }
    });
    console.log('access');
}).listen(8080);
console.log('Server running.....');